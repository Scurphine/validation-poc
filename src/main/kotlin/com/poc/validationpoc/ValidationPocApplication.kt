package com.poc.validationpoc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory

@SpringBootApplication
class ValidationPocApplication

fun main(args: Array<String>) {
    runApplication<ValidationPocApplication>(*args)
}
