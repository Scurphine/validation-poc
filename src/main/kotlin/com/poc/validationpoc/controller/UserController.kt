package com.poc.validationpoc.controller

import com.poc.validationpoc.model.User
import com.poc.validationpoc.service.UserService
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.ConstraintViolationException
import javax.validation.Valid

@RestController
@RequestMapping("api/user")
class UserController(val userService: UserService) {

    @GetMapping
    fun findAll() = userService.findAll()

    @GetMapping("/{id}")
    fun getOne(@PathVariable id: Long) = userService.getOne(id)

    @PostMapping
    fun create(@Valid @RequestBody user: User) = userService.create(user)

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long, @Valid @RequestBody user: User) = userService.update(id, user)

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = userService.delete(id)
}