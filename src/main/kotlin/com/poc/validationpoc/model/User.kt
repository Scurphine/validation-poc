package com.poc.validationpoc.model

import com.poc.validationpoc.model.UserType.NOBODY
import com.poc.validationpoc.validation.AllowedUserTypes
import com.poc.validationpoc.validation.NotBlankForUserType
import org.springframework.validation.annotation.Validated
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType.STRING
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@Entity
@Validated
@Table(name = "USER")
@NotBlankForUserType
class User(
        @get:Id
        @get:GeneratedValue(strategy = IDENTITY)
        @get:Column(name = "ID")
        var id: Long? = null,

        @get:NotBlank
        @get:Column(name = "USERNAME")
        var username: String = "",

        @get:Size(min = 8, max = 16)
        @get:Column(name = "PASSWORD")
        var password: String = "",

        @get:Enumerated(STRING)
        @get:AllowedUserTypes(allowedValues = [UserType.ADMIN, UserType.USER, UserType.GUEST])
        @get:Column(name = "TYPE")
        var type: UserType = NOBODY,

        @get:Column(name = "SPECIAL_TEXT")
        var specialText: String?
)