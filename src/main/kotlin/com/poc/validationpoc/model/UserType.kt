package com.poc.validationpoc.model

enum class UserType {
    ADMIN,
    USER,
    GUEST,
    NOBODY
}