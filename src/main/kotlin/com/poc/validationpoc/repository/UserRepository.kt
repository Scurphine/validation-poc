package com.poc.validationpoc.repository

import com.poc.validationpoc.model.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Long>