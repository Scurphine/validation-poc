package com.poc.validationpoc.service

import com.poc.validationpoc.model.User
import com.poc.validationpoc.repository.UserRepository
import org.springframework.stereotype.Service

@Service
class UserService(val userRepository: UserRepository) {
    fun findAll(): List<User> = userRepository.findAll()

    fun getOne(id: Long): User? = userRepository.getOne(id)

    fun create(user: User) =
            if (user.id == null)
                userRepository.save(user)
            else
                throw IllegalStateException()


    fun update(id: Long, user: User) = getOne(id)?.let {
        userRepository.save(it.apply { this.id = id })
    } ?: throw IllegalStateException()

    fun delete(id: Long) = userRepository.deleteById(id)
}