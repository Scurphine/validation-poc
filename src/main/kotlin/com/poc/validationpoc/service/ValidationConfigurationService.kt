package com.poc.validationpoc.service

import com.poc.validationpoc.model.UserType
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class ValidationConfigurationService(
        @Value("\${validation.usertype}") private val userType: UserType
) {
    fun getType() = userType
}