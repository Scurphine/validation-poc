package com.poc.validationpoc.validation

import com.poc.validationpoc.model.UserType
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [AllowedUserTypesValidator::class])
annotation class AllowedUserTypes(
        val message: String = "",
        val groups: Array<KClass<*>> = [],
        val payload: Array<KClass<out Payload>> = [],
        val allowedValues: Array<UserType> = []
)

class AllowedUserTypesValidator : ConstraintValidator<AllowedUserTypes, UserType> {
    private lateinit var allowedValues: Array<UserType>
    private val errorMessage
        get() = "value should be one of ${allowedValues.joinToString()}"

    override fun initialize(constraintAnnotation: AllowedUserTypes) {
        allowedValues = constraintAnnotation.allowedValues
    }

    override fun isValid(value: UserType?, context: ConstraintValidatorContext) = allowedValues
            .contains(value)
            .apply {
                if (!this) {
                    context.buildConstraintViolationWithTemplate(errorMessage)
                            .addConstraintViolation()
                }
            }
}