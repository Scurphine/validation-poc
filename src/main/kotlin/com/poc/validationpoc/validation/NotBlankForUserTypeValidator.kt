package com.poc.validationpoc.validation

import com.poc.validationpoc.model.User
import com.poc.validationpoc.service.ValidationConfigurationService
import org.springframework.stereotype.Component
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [NotBlankForUserTypeValidator::class])
annotation class NotBlankForUserType(
        val message: String = "",
        val groups: Array<KClass<*>> = [],
        val payload: Array<KClass<out Payload>> = []
)

@Component
class NotBlankForUserTypeValidator(
        private val validationService: ValidationConfigurationService
) : ConstraintValidator<NotBlankForUserType, User> {
    private val errorMessage
        get() = "User of type ${validationService.getType()} should have specialText value specified"

    override fun isValid(value: User?, context: ConstraintValidatorContext) = value?.let {
        if (it.type == validationService.getType()) {
            it.specialText != null
        } else true
    }?.apply {
        if (!this) {
            context.buildConstraintViolationWithTemplate(errorMessage).addConstraintViolation()
        }
    } ?: true
}